var fs = require('fs');
var path = require("path");

module.exports = {

  // Helper function to read a file
  readFile: function (file, encoding) {
    fs.readFile(file, encoding, function (err, data) {
      if (err) {
        return console.log(err)
      }
      return data
    });
  },
};
