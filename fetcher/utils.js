var fs = require('fs');
var http = require('http');
var mkdirp = require('mkdirp');

module.exports = {

  // Helper function to pad zeros in front
  pad: function (num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  },

  // Helper function to create directory structure
  createDir: function (dir) {
    mkdirp(dir, function (err) {
      if (err) {
        return console.error(err);
      }
    });
  },

  // Helper function to write to a file
  writeFile: function (file, data) {
    fs.open(file, 'w+', function(err) {
      if (err) {
        return console.error(err);
      }
    });
    fs.writeFile(file, JSON.stringify(data), function(err) {
      if (err) {
        return console.error(err);
      }
    });
  },
};
