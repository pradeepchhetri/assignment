### Assignment

This repository consists of two components:

1. **fetcher** - crawls the [portal](https://publicrecordsaccess.fultoncountyga.gov/Portal/) using [segmentio/nightmare](https://github.com/segmentio/nightmare)

2. **indexer** - reads crawled data and indexes into database

#### Fetcher

To execute the fetcher, you need to first install the dependencies:

```
$ npm install
```

Execute the crawler **without** downloading documents:

```
$ node fetcher/crawl.js
```

Execute the crawler **with** downloading documents:

```
$ node fetcher/crawl.js --downloadDocs
```

#### Indexer

You need to setup CouchDB before executing the indexer. To run the indexer:

```
$ node indexer/index.js
```

This will start reading the crawled data and indexing them into CouchDB
